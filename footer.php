
<footer class="footer justify-content-center" role="contentinfo">
    <div class="container-fluid">
        <div class="row">
            <div class=" col-md-5 col-12">
                <div class="left-footer">
                    <div class="brand-block-footer">
                        <a href="<?php echo get_home_url(); ?>">
                        <span class="logo d-block">
                            <img src="<?php echo get_template_directory_uri() . "/img/logo_footer.png" ?>">
                        </span>
                        </a>
                    </div>
                    <div class="sign-block-footer">
                        <span class="sign-logo">2020 © infiberry, s.r.o.<br></span>
                        <span class="sign-esoul">powered by <a href="https://e-soul.cz/" target="_blank">E-Soul</a></span>
                    </div>
                </div>
            </div>
            <div class="offset-md-1 col-md-3 col-sm-6 col-12">
                <div class="about-footer">
                    <p class="about-footer-header"><?php _e('O nákupu','eso-theme'); ?></p>
                    <a href="<?php echo get_page_link( 14 ); ?>" class="d-block footer-item"><span><?php _e('Obchodní podmínky', 'eso-theme');?></span></a>
                    <a href="#" target="_blank" class="d-block footer-item"><span><?php _e('Platba a doprava', 'eso-theme'); ?></span></a>
                    <a href="#" target="_blank" class="d-block footer-item"><span><?php _e('Ochrana osobních údajů', 'eso-theme'); ?></span></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-12">
                <div class="kontakt-footer">
                    <p class="kontakt-footer-header"><?php _e('Kontakt', 'eso-theme');?></p>
                    <a href="mailto:info@frusack.com" target="_blank" class="d-block footer-item"><span>info@frusack.com</span></a>
                    <a href="tel:+420606243776" target="_blank" class="d-block footer-item"><span>+420 606 243 776</span></a>
                    <div class="kontakt-footer-icons">
                        <div class="footer_fb d-inline-block mr-2">
                            <a href="https://www.facebook.com/frusack/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/fb_icon.png"/></a>
                        </div>
                        <div class="footer_inst d-inline-block">
                            <a href="https://www.instagram.com/frusack/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/inst_icon.png"/></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset-md-1"></div>
        </div>
    </div>
</footer>

<?php if ( current_user_can( 'install_plugins' ) ) : ?>
    <div id="show-frontend-dashboard" class="btn btn-primary"><i class="fas fa-tachometer-alt"></i> <?php _e("Přehled", "eso-theme") ?></div>
    <div id="eso-frontend-dashboard"></div>
<?php endif; ?>

</div>

<?php wp_footer(); ?>
<?php do_action( 'eso_body_end_hook' ); ?>
</body>
</html>
