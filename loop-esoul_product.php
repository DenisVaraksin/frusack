<?php
$catId = null;
if(isset($_GET["category_id"])) {
    $catId = (int) $_GET["category_id"];
} ?>
<div class="col-12 mb-5">
    <form class="form-inline" method="get">
        <div name="category_id" class="categories-test">

            <a class="<?php if(!isset($_GET["category_id"])) :
            echo 'active-link';
            endif;?> category-link" href="<?php echo get_post_type_archive_link("esoul_product") ?>">Vše </a>&nbsp;
            <?php
            foreach ( get_terms( ["taxonomy" => "product_category", "hide_empty" => false ] ) as $category ) {
                // firstly - check for the active URL to highlight the menu
                // and if category is not Ostatni with ID = 31
                if ($category->term_id !== 31) {
                ?>
                <a href="?category_id=<?php echo $category->term_id ?>" class='<?php if($catId == $category->term_id) { ?>active-link <?php } ?> category-link'><?php echo $category->name ?> </a>&nbsp;
            <?php }
                } ?>
            <!-- Displaying Category Ostatni wiht ID 31 as a last one-->
            <a href="?category_id=31" class='<?php if($catId == 31) { ?>active-link <?php } ?> category-link'><?php echo 'Ostatní'; ?> </a>&nbsp;
        </div>
    </form>
</div>

<!-- Callin out products-->
<?php
$args = [
	"post_type"   => "esoul_product",
	"post_status" => "publish"
];

( ! empty( $_GET["category_id"] ) ) ? $args["tax_query"] = [
	[
		"taxonomy" => "product_category",
		"terms"    => (int) $_GET["category_id"]
	]
] : null;

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ):
	while ( $the_query->have_posts() ) : $the_query->the_post();

		$product = new Eso_Product( get_the_ID() ); ?>
        <div class="col-sm-6 col-md-6 col-lg-4 mb-3">                           <!-- col-sm-6 col-md-4 col-lg-4 mb-3 -->
                <?php echo $product->get_list_item_template(); ?>
        </div>
            <?php

	endwhile;
else: ?>



    <h2><?php _e( 'Zatím žádné produkty.', 'eso-theme' ); ?></h2>
<?php endif; ?>
