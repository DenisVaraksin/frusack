<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
			echo ' – ';
		} ?><?php bloginfo( 'name' ); ?></title>
    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<?php if ( get_option( "eso_store_favicon" ) ) : $favicon_id = get_option( "eso_store_favicon" ); ?>
        <link rel="apple-touch-icon" sizes="180x180"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 180, 180 ] );
		      echo $favicon[0] ?>">
        <link rel="icon" type="image/png" sizes="192x192"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 192, 192 ] );
		      echo $favicon[0] ?>">
        <link rel="icon" type="image/png" sizes="96x96"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 90, 90 ] );
		      echo $favicon[0] ?>">
	<?php endif; ?>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">

	    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KG73HHL');</script>
	<!-- End Google Tag Manager -->
	<meta name="google-site-verification" content="kezGlyVTAlGnajfC76tNh7_j3YTZlMxp3rHJsfXvAto" />
    <!-- Google Search Console -->
    <meta name="google-site-verification" content="6e_Gg8Do6zhgBb5LH03XjsoeCcYQQjInbFqaS21MmBk" />

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'eso_body_start_hook' ); ?>
<?php wp_nonce_field( ESO_NONCE ); ?>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KG73HHL"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">
    <!-- fellscreen nav menu -->
    <header class="header clear" role="banner">
        <div id="full-navbar-container" class="container-fluid">
            <div id="fullscreen-menu" class="row">
                <div class="col-md-5">
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'header-menu',
                        'menu_class'      => 'navbar-nav m-auto navbar-fullscreen',
                        'depth'           => 2,
                        'container'      => 'main-menu-container',
                        'container_class' => 'navbar-collapse',
                        'container_id'    => 'bs-example-navbar-collapse-1',
                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'          => new WP_Bootstrap_Navwalker()
                    ) ); ?>
                </div>
                <div class="col-md-2">
                    <div class="brand-block">
                        <a href="<?php echo get_home_url(); ?>" class="navbar-brand logo-smallscreen ">
                            <span class="logo d-block">
                                <img src="<?php echo get_template_directory_uri() . "/img/frusack_logo1.png" ?>">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'header-menu-2',
                        'menu_class'      => 'navbar-nav m-auto navbar-fullscreen',
                        'depth'           => 2,
                        'container'      => 'main-menu-2-container',

                        'container_class' => 'navbar-collapse',
                        'container_id'    => 'bs-example-navbar-collapse-1',
                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'          => new WP_Bootstrap_Navwalker()
                    ) ); ?>
                </div>
                <div class="col-md-1" id="header-last-md">
                    <div class="language-switcher"> <?php  pll_the_languages(array(  )); ?> </div>  <!-- Language switcher from Polylang  -->

                    <ul id="last-nav-element" class="navbar-nav ml-auto mr-lg-3">

                        <li class="nav-item menu-item menu-cart">
                            <a href="<?php echo home_url( '/cart' ) ?>" class="nav-link nav-link-cart">
                                <i class='fas fa-shopping-cart' title='<?php __( 'Košík', 'eso' ) ?>'></i>
                                <span class='d-lg-none'><?php __( 'Košík', 'eso' ) ?></span>
                                <span id="cart-count">
                                    <?php $cart = new Eso_Cart( eso_session_token() );
                                    echo $cart->get_items_count();
                                    ?>
                                 </span>
                            </a>
                        </li>
                        <li class="nav-item menu-item menu-account dropdown menu-item-has-children">
                            <a href="#" data-toggle="dropdown" aria-haspopup='true' aria-expanded='false' class='dropdown-toggle nav-link' >
                                <!--<i class='far fa-user' title='<?php _e( 'Můj účet', 'eso' ) ?>'></i> -->
                                <img class="nav-picture-account" title='<?php _e( 'Můj účet', 'eso' ) ?>' src="<?php echo get_stylesheet_directory_uri() ?>/img/user.png"/>
                                <span class='d-lg-none'><?php __( 'Můj účet', 'eso' ) ?></span>
                            </a>
                            <ul class="dropdown-menu my-account-menu-list">
                                <li class="nav-item menu-item">
                                    <?php
                                    $customer = new Eso_Customer( get_current_user_id() );
                                    if ( is_user_logged_in() ) : ?>
                                        <a href="<?php eso_the_page_link( 'account' ); ?>" class="nav-link">
                                            <?php echo $customer->get_shipping_first_name(); ?>
                                        </a>
                                    <?php endif ?>
                                </li>

                                <?php if ( is_user_logged_in() ) : ?>
                                    <li class='menu-item nav-item '>
                                        <a href="<?php eso_the_page_link( 'account' ); ?>"
                                           class="dropdown-item"><?php _e( "Můj účet", "eso" ) ?></a>
                                    </li>
                                    <li class="menu-item nav-item ">
                                        <a href="<?php eso_the_page_link( 'orders' ); ?>"
                                           class="dropdown-item"><?php _e( "Objednávky", "eso" ) ?></a>
                                    </li>
                                    <li class="menu-item nav-item ">
                                        <a href="<?php echo wp_logout_url(); ?>"
                                           class="dropdown-item"><?php _e( "Odhlásit se", "eso" ) ?></a>
                                    </li>
                                <?php else : ?>
                                    <li class='menu-item nav-item '>
                                        <a href="<?php eso_the_page_link( 'login' ); ?>"
                                           class="dropdown-item"><?php _e( "Přihlásit se", "eso" ) ?></a>
                                    </li>
                                <?php endif ?>


                            </ul>
                        </li>

                    </ul>

                    <?php echo do_shortcode( '[eso_currency_switcher]' ); ?>
                    <!--<form class="form-inline my-2 my-lg-0">-->
                    <!----><?php //get_template_part( 'searchform' ); ?>
                    <!-- </form>-->
                    </ul>

                </div>
            </div>


            <!-- fullscreen nav menu ends -->
            <!-- -----------------------  -->
            <!-- smallscreen nav menu -->
            <div id="smallscreen-menu" class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a href="<?php echo get_home_url(); ?>" class="navbar-brand logo-smallscreen ">
                            <span class="logo d-block">
                                <img src="<?php echo get_template_directory_uri() . "/img/frusack_logo1.png" ?>">
                            </span>
                        </a>


                        <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">

                            <?php wp_nav_menu( array(
                                'theme_location'  => 'header-menu',
                                'menu_class'      => 'navbar-nav m-auto',
                                'depth'           => 2,
                                'container'      => ''
                                /*
                                'container_class' => 'navbar-collapse',
                                'container_id'    => 'bs-example-navbar-collapse-1',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',*/
                                /*'walker'          => new WP_Bootstrap_Navwalker()*/
                            ) ); ?>

                            <?php wp_nav_menu( array(
                                'theme_location'  => 'header-menu-2',
                                'menu_class'      => 'navbar-nav m-auto',
                                'depth'           => 2,
                                'container'      => ''
                                /*
                                'container_class' => 'navbar-collapse',
                                'container_id'    => 'bs-example-navbar-collapse-1',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'          => new WP_Bootstrap_Navwalker()*/
                            ) ); ?>


                            <ul id="language-switcher-small" class="navbar-nav ml-auto mr-lg-3">
                                <li class="nav-item menu-item menu-cart">
                                    <a href="<?php echo home_url('/cart') ?>" class="nav-link">
                                        <i class='fas fa-shopping-cart' title='<?php __('Košík', 'eso') ?>'></i>
                                        <span class='d-lg-none'><?php __('Košík', 'eso') ?></span>

                                        <span id="cart-count">
                                            <?php $cart = new Eso_Cart( eso_session_token() );
                                            echo $cart->get_items_count();
                                            ?>
                                        </span>
                                    </a>
                                </li>

                                <li class="nav-item menu-item menu-account dropdown menu-item-has-children pb-1">
                                    <a href="#" data-toggle="dropdown" aria-haspopup='true' aria-expanded='false'
                                       class='dropdown-toggle nav-link'>
                                       <!-- <i class='far fa-user' title='<?php _e( 'Můj účet', 'eso' ) ?>'></i> -->
                                        <img class="nav-picture-account" title='<?php _e( 'Můj účet', 'eso' ) ?>' src="<?php echo get_stylesheet_directory_uri() ?>/img/user.png"/>
                                        <span class='d-lg-none'><?php __( 'Můj účet', 'eso' ) ?></span></a>
                                    <ul class="dropdown-menu my-account-menu-list">
                                        <li class="nav-item menu-item">
                                            <?php
                                            $customer = new Eso_Customer(get_current_user_id());
                                            if (is_user_logged_in()) : ?>
                                                <a href="<?php eso_the_page_link('account'); ?>" class="nav-link">
                                                    <?php echo $customer->get_shipping_first_name(); ?>
                                                </a>
                                            <?php endif ?>
                                        </li>

                                        <?php if ( is_user_logged_in() ) : ?>
                                            <li class='menu-item nav-item'>
                                                <a href="<?php eso_the_page_link( 'account' ); ?>"
                                                   class="dropdown-item"><?php _e( "Můj účet", "eso" ) ?></a>
                                            </li>
                                            <li class="menu-item nav-item">
                                                <a href="<?php eso_the_page_link( 'orders' ); ?>"
                                                   class="dropdown-item"><?php _e( "Objednávky", "eso" ) ?></a>
                                            </li>
                                            <li class="menu-item nav-item">
                                                <a href="<?php echo wp_logout_url(); ?>"
                                                   class="dropdown-item"><?php _e( "Odhlásit se", "eso" ) ?></a>
                                            </li>
                                        <?php else : ?>
                                            <li class='menu-item nav-item'>
                                                <a href="<?php eso_the_page_link( 'login' ); ?>"
                                                   class="dropdown-item"><?php _e( "Přihlásit se", "eso" ) ?></a>
                                            </li>
                                        <?php endif ?>
                                    </ul>
                                </li>




                                <div class="language-switcher-small"> <?php  pll_the_languages(array(  )); ?> </div>  <!-- Language switcher from Polylang  -->
                            <?php echo do_shortcode('[eso_currency_switcher]'); ?>
                            <!--<form class="form-inline my-2 my-lg-0">-->
                            <!----><?php //get_template_part( 'searchform' ); ?>
                            <!--</form>-->
                        </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- smallscreen nav menu ends -->
        </div>
    </header>