<?php
if ( ! is_user_logged_in() ) :
	get_template_part( "pages/login" );
else :
	$customer = new Eso_Customer( get_current_user_id() );
	$fields = new Eso_Fields();

	get_template_part( "pages/header" );
	?>
    <form class="ajax-form">
        <input type="hidden" name="action" value="eso_ajax"/>
        <input type="hidden" name="eso_action" value="save_customer_data"/>
        <input type="hidden" name="eso_success_callback" value="render_saved_notification"/>
        <input type="hidden" name="eso_callback_target" value="#form-result" />
        <input type="hidden" name="customer_id" value="<?php echo $customer->get_id() ?>"/>
        <div id="account" class="container">
            <div class="row">
                <div class="col-md-4">
					<?php $fields->form_group_input( "fields[shipping_email]", "Email", $customer->get_email(), null, "email", [ "readonly" => "readonly", "id" => "shipping_email" ] ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
					<?php $fields->form_group_input( "fields[shipping_first_name]", "Křestní jméno", $customer->get_shipping_first_name() , null); ?>
                </div>
                <div class="col-md-4">
					<?php $fields->form_group_input( "fields[shipping_last_name]", "Příjmení", $customer->get_shipping_last_name(), null ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
					<?php $fields->form_group_input( "fields[shipping_phone]", "Telefon", $customer->get_phone(), null, "tel", ["id" => "shipping_phone"] ); ?>
                </div>
                <hr>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4><?php _e( "Adresa", "eso" ) ?></h4>
                </div>
                <div class="col-md-3">
					<?php $fields->form_group_input( "fields[shipping_street]", "Ulice a č.p.", $customer->get_shipping_street() ); ?>
                </div>
                <div class="col-md-3">
					<?php $fields->form_group_input( "fields[shipping_city]", "Město", $customer->get_shipping_city() ); ?>
                </div>
                <div class="col-md-2">
					<?php $fields->form_group_input( "fields[shipping_postcode]", "PSČ", $customer->get_shipping_postcode() ); ?>
                </div>
                <div class="col-md-3">
                    <label><?php _e( "Země", "eso" ) ?></label>
					<?php $fields->render_country_select( "fields[shipping_country]", "Země", true, $customer->get_shipping_country_code() ); ?>
                </div>

                <div class="col-md-12">
                    <hr class="mt-4 mb-4">
					<?php $fields->checkbox( "fields[billing_on]", "Nakupuji na firmu (IČ, DIČ)", $customer->is_billing_on(), 1, [
						"data-toggle" => "collapse",
						"data-target" => "#billing_fields"
					] ) ?>

                    <div id="billing_fields" class="collapse <?php if ( $customer->is_billing_on() ) {
						echo "show";
					} ?>">
                        <h3><?php _e( "Fakturační údaje", "eso" ) ?></h3>
                        <div class="row">
                            <div class="col-md-4">
								<?php $fields->form_group_input( "fields[billing_company_name]", "Jméno / Název firmy", $customer->get_billing_company_name() ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
								<?php $fields->form_group_input( "fields[billing_ico]", "IČO", $customer->get_billing_ico() ); ?>
                            </div>
                            <div class="col-md-4">
								<?php $fields->form_group_input( "fields[billing_dic]", "DIČ", $customer->get_billing_dic() ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
								<?php $fields->form_group_input( "fields[billing_street]", "Ulice a č. p.", $customer->get_billing_street() ); ?>
                            </div>
                            <div class="col-md-4">
								<?php $fields->form_group_input( "fields[billing_city]", "Město", $customer->get_billing_city() ); ?>
                            </div>
                            <div class="col-md-2">
								<?php $fields->form_group_input( "fields[billing_postcode]", "PSČ", $customer->get_billing_postcode() ); ?>
                            </div>
                            <div class="col-md-3">
                                <label><?php _e( "Země", "eso" ) ?></label>
								<?php $fields->render_country_select( "fields[billing_country]", "Země", true, $customer->get_billing_country_code() ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-4">
                    <button type="submit" class="btn btn-secondary"><?php _e( "Uložit", "eso" ) ?></button>
                </div>
            </div>
        </div>
    </form>

<?php endif; ?>