<div class="container">
	<div class="row">
		<div class="col-md-4 offset-md-4">
			<h1><?php the_title() ?></h1>
			<?php echo do_shortcode("[eso_lost_password]") ?>
		</div>
	</div>
</div>