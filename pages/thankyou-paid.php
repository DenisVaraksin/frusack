<div class="container">
    <div class="row">
        <div class="col text-center">
			<?php the_content(); ?>
            <div id="order_detail" class="row">
                <div class="col-md-12">
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Číslo objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_id(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Datum objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_date_created(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Email", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_shipping_email(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Cena celkem", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_total(true); ?></p>
                        </div>
                    </div>
                    <hr/>
                </div>

                <div class="col text-center mb-3 mt-2">
                    <a href="<?php echo home_url() ?>" class="btn btn-link btn-sm mr-4"><?php _e("Na hlavní stránku", "frusack") ?></a>
                    <a class="btn btn-secondary btn-sm"
                       href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>" target="_blank">
			            <?php _e( "Zobrazit fakturu", "eso" ) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>