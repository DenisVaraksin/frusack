<?php get_header(); ?>

<main role="main">


    <div class="container col-sm-6 col-md-4 col-lg-4 mt-5 mb-5">

		<?php if ( have_posts() ) : ?>
        <div class="row justify-content-center">
            <h1 class="has-large-font-size font-weight-bold"><?php printf( esc_html__( 'Výsledky hledání pro: %s', 'default' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </div>
    </div>

    <!-- found items -->
    <div class="container">
        <div class="row">
			<?php while ( have_posts() ) : the_post();
				if ( get_post_type() == "esoul_product" ) :
					$product = new Eso_Product( get_the_ID() );
				    echo $product->get_list_item_template(); ?>
				<?php else : ?>
                    <div class="col-sm-12">
                        <a href="<?php the_permalink(); ?>" class="card__wrap">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title"><?php the_title(); ?></h3>
                                </div>
                            </div>
                        </a>
                    </div>
				<?php endif; ?>
			<?php endwhile;
			else: ?>
                <h2><?php _e( 'Zatím žádné produkty.', 'eso-theme' ); ?></h2>
			<?php endif; ?>
        </div>
    </div>
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
