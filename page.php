<?php get_header(); ?>
<main role="main">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
	    <?php
        switch(get_the_ID()) {
            case eso_get_page_id('cart'):
	            get_template_part('templates/cart/cart');
                break;
            case eso_get_page_id('login'):
	            get_template_part('pages/login');
                break;
            case eso_get_page_id('lost-password'):
                get_template_part('pages/lost-password');
                break;
            case eso_get_page_id('set-password'):
                get_template_part('pages/set-password');
                break;
            case eso_get_page_id('register'):
	            get_template_part('pages/register');
                break;
            case eso_get_page_id('thankyou'):
	            get_template_part('pages/thankyou');
                break;
            case eso_get_page_id('orders'):
	            get_template_part('pages/orders');
                break;
            case eso_get_page_id('account'):
                get_template_part('pages/account');
                break;
            default:
	            get_template_part('pages/default');
        };
    endwhile; endif; ?>
</main>
<?php get_footer(); ?>
