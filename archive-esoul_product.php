<?php get_header(); ?>

<main role="main">
	<div class="container" id="frusack-product-list">

		<div class="row">
			<div class="col p-5 m-5 text-center">
				<h1 class="has-large-font-size font-weight-bold custom-check"><?php _e( 'Produkty', 'eso-theme' ); ?></h1>
			</div>
		</div>

		<div class="row">
			<?php get_template_part('loop-esoul_product'); ?>
		</div>

		<div class="row">
			<div class="col">
				<?php get_template_part('pagination'); ?>
			</div>
		</div>


	</div>

</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
