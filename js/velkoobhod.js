(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        //Velkoobhod Slider

        //another try with the slide
        $('.velko-slides-wrap').slick({
            centerMode: true,
            //'<i class="fa fa-arrow-right"></i>',
            slidesToShow: 3,
            centerPadding: 0,
            infinite: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            dots: false,

            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        fade: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        fade: true
                    }
                }
            ]
        });

       //displazing dot of displazed elements


       //------------------------------------------------

        //Switching from form 1 to form 2
        $('.open-model-2').click(function() {
            $('#velkoModal1').modal('hide');
            $('#velkoModal2').modal('show');
        });



        // Hiding Form-2 and showinf Form-3 if mail sent
        var wpcf7Form = document.querySelector( '.wpcf7' );
        console.log(wpcf7Form);

        wpcf7Form.addEventListener( 'wpcf7mailsent', function( event ) {
            $('#velkoModal2').modal('hide');
            $('#velkoModal3').modal('show');
        }, false );



        //change image on Velkoobhod advantages image
        $(".velko-advantages-image").hover(function(){
                $(this ).find('img').addClass('d-none');
                $(this).find('img').next().removeClass('d-none');
            },
            function() {
                $(this).find('img').removeClass('d-none');
                $(this).find('img').next().addClass('d-none');
            });

    });

})(jQuery, this);