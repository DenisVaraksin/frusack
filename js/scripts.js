(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        $(".eso-product-quantity").change(function () {
            let new_value = $(this).val();
            $(this).attr("value", new_value);
        });

        $('.homeslider').slick({
            infinite: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: false,
            nextArrow: false
        });

        //Function to display/hide blocks at "O Frusacky" page
        function theDisplayFunction() {

        }


        //open some links with _blank atribute
        //$('a[href^="https://shop.frusack.com"]').attr('target','_blank');

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        // Sticky header on scroll functionality to add
        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};

        // Get the header
        var header = document.getElementById("full-navbar-container");
        var content = document.getElementById("homepage-slider-container");
        var content1 = document.getElementById("about-frusack-header-container-1");

        // Get the offset position of the navbar
        var sticky = header.offsetTop;

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset > sticky) {
                $(header).addClass("sticky");
                $(content).addClass("homepage-slider-margin");
                $(content1).addClass("homepage-slider-margin");
            } else {
                $(header).removeClass("sticky");
                $(content).removeClass("homepage-slider-margin");
                $(content1).removeClass("homepage-slider-margin");
            }
        }

        //display DETAILS on product picture hover
        $('.product-item__image').hover(function() {
            $( this ).find('.product-item__image-hover').removeClass( "d-none" );
        }, function() {
            $( this ).find('.product-item__image-hover').addClass( "d-none" );
        });

        //removing title atributes from Navigation Menu
        $('#menu-main-menu li a').removeAttr('title');
        $('#menu-main-menu-2 li a').removeAttr('title');

        //About Frusack add-remove class on button click
        $('.about-content-button').on('click', function() {

            let $class = $(this).attr('data-attr');

            $('.about-content-article').addClass('d-none');
            $('.about-content-picture').addClass('d-none');
            $('.about-content-text').addClass('d-none');
            $('.'+ $class).removeClass('d-none');
            $('.about-content-button').removeClass('active-button');

            // let testVar = $(this).parent().parent().prev().prev().children().addClass("show active-content");
            // console.log(testVar);

            $(this).addClass("active-button");
        });

        // Single product active class for opened menu
        $('.single-product-button').on('click', function() {
            $(this).toggleClass("active-button");
        });

        //making Form with a single product for Home Page ///
       // $('.product-item').on('click', function(e){
           // e.preventDefault();
           // $('#theProductModal').modal('show').find('.modal-content').load($(this).attr('href'));
       // });


        // Disable on.click event for the selection product amounts
        $(".card__wrap .product-details__label").click(function(e) {
            e.preventDefault();
        });


    });
})(jQuery, this);