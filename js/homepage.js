(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        //Homepage Slider
        $('#homepage-slider').slick({
            //infinite: true,
            dots: true,
            arrows: true,
            slidesToShow: 1,
            prevArrow: false,
            nextArrow: $("#slickRight_main_home2 button"),
            fade: true,
            cssEase: 'linear',
            speed: 500,
        });

        $('.product-color-trigger').click(function(e) {
            //after product color clicked
            e.preventDefault();
            let image = $(this).children();
            let imageCont = $(this).parent().parent();

            //changing active color styling
            if (image.hasClass('product-color-image-border')) {
                console.log('do nothing');
            } else {
                imageCont.find('img').removeClass('product-color-image-border');
                image.addClass('product-color-image-border');
            }

            let id = $(this).data('target') ;
            const categoryWrapper = $(this).closest('.product-category-wrap');
            const addToCartForm = categoryWrapper.find('.add-to-cart-form');

            //changing ID on the form
            addToCartForm.attr("data-id", id);
            categoryWrapper.find('.product-item').addClass('d-none');

           $('#product-item-' + id ).removeClass('d-none');

        });

    });

})(jQuery, this);