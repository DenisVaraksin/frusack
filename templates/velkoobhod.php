<?php
/*
Template Name: Velkoobhod
Template Post Type: page
*/
get_header();?>

    <main role="main">


<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <section id="velkoobhod-frusack">
        <!-- content is being rendered in Gutenberg Main Page -->
        <?php the_content(); ?>
    </section>
    </main>

<?php  endwhile; endif; ?>

<?php get_footer(); ?>