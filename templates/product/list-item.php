<?php
/**
 * @since 2019.7
 *
 * @var $this Eso_Product
 */
?>


            <a class="card__wrap product-item">
                <div class="card-product-item-wrapper">
                    <button type="button" class="modal-button" data-target="#theProductModal-<?php echo get_the_ID(); ?>" data-toggle="modal">
                        <div class="product-item__image">
                            <?php if ( $this->get_featured_image_id() ) {
                                $this->the_featured_image();
                            } else {
                                echo eso_empty_thumbnail();
                            }
                            ?>
                            <div class="product-item__image-hover d-none">
                                <p>detail</p>
                                <p class="bigger">+</p>
                            </div>
                        </div>
                    </button>


                    <?php
                    if($this->get_tag_id()) {
                        $product_tag = new Eso_Product_Tag( $this->get_tag_id() );
                        $product_tag->render("product-item");
                    }
                    ?>
                    <div class="product-item-title-wrap">
                        <h3 class="product-item__title"><?php echo $this->get_name(); ?></h3>
                    </div>

                    <div class="price price--list">
                        <?php if ( $this->is_discounted( eso_get_active_currency() ) ) : ?>
                            <span class="price-before-discount"><?php echo $this->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
                        <?php endif; ?>
                        <span class="price__item"><?php echo $this->get_price( eso_get_active_currency(), true, true ); ?></span>

                    </div>

                    <div class="product-item__purchase">
                        <div class="product-colors <?php $category = $this->get_category_name();
                            echo $category . 'Modified'; ?>">
                            <img type="" class="color-actual-image"
                                 src="<?php echo $this->get_symbol_image_url(); ?>"
                                 alt="product-color">
                        </div>

                        <div class="product-purchase">
                            <form action="" class="add-to-cart-form"
                                  data-id="<?php echo $this->get_id() ?>">
                                <?php wp_nonce_field( ESO_NONCE ); ?>

                                    <div class="product-details__label-wrap">
                                        <label class="product-details__label">
                                            <input type="number"
                                                   class="form-control form-control-lg eso-product-quantity"
                                                   name="eso-product-quantity" min="1" required value="1"
                                                   data-product-name="<?php echo $this->get_name() ?>"
                                                   max="<?php echo $this->get_stock_amount() ?>"/>
                                        </label>
                                    </div>
                                    <div class="product-details__button-wrap">
                                        <button type="submit" class="btn-custom-white add-to-cart">
                                            <?php _e( "do košíku", "eso" ) ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/shop-cart.png" alt="">
                                        </button>
                                    </div>

                            </form>

                        </div>
                    </div>

                </div>
                <!-- comment out link from the details !! (for now) -->
            </a>

        <!-- Modal window for a product quick view  -->
        <div class="modal fade" id="theProductModal-<?php echo get_the_ID(); ?>">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md">
                                <?php get_template_part('templates/blocks/products/modal-product'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




