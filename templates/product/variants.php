<?php
/**
 * Render Eso_Product_Variant for template for Frusack theme
 *
 * @since 2020.2.17
 * @var $variants array
 * @var $variant Eso_Product_Variant
 */
foreach ( $variants as $variant_slug => $variant_data ) {
	$variant = new Eso_Product_Variant( $variant_slug, $this );
	?>
    <div class="form-row mb-3 d-flex align-items-center">
        <div class="col-4 col-lg-3">
            <label for="<?php echo $variant->get_slug() ?>" class="mb-0 font-weight-bold text-muted">
				<?php echo $variant->get_name() ?>
            </label>
        </div>
        <div class="col-4">
            <select name="<?php echo $variant->get_slug() ?>" class="form-control"
                    id="<?php echo $variant->get_slug() ?>">
				<?php foreach ( $variant->get_attributes() as $attribute ) : ?>
                    <option value="<?php echo $attribute["slug"] ?>"><?php echo $attribute["slug"] ?></option>
				<?php endforeach; ?>
            </select>
        </div>
    </div>
<?php }
?>

