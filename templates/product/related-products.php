<?php
/**
 * Render related products of product.
 *
 * @since 2020.2.17
 *
 * @var $products array
 * @var $product Eso_Product
 */

if ( ! empty( $products ) ) { ?>
    <div class="row">
	    <div class="col mt-5 mb-4">
		    <h3><?php _e("Další produkty", "frusack") ?></h3>
	    </div>
    </div>
    <div class="row">
		<?php
		foreach ( $products as $product_id ) {
			$product = new Eso_Product( $product_id ); ?>
            <div class="col-sm-6 col-md-4 col-lg-4 mb-3">
				<?php echo $product->get_list_item_template(); ?>
            </div>
		<?php } ?>
    </div>
<?php }