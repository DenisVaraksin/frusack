<?php
/**
 * This template is sent to store owner when a new order is made
 *
 * @var $object_id
 */
$order = new Eso_Order( $object_id );
$store = new Eso_Store();

eso_email_template_header( __( "Nová objednávka na e-shopu", "eso" ) ); ?>
	<h1><?php _e( "Nová objednávka na e-shopu ", "eso" ) ?> <?php echo $store->get_name(); ?></h1>
	<table>
		<tr class="head">
			<td></td>
			<td><?php _e("Název položky", "eso") ?></td>
			<td><?php _e("Cena za ks", "eso") ?></td>
			<td><?php _e("Množství", "eso") ?></td>
			<td><?php _e("Celková cena", "eso") ?></td>
		</tr>
		<?php
		/* @var $item Eso_Order_Item */
		foreach ( $order->get_items() as $item ) : ?>
			<tr>
				<td><?php $item->get_product()->the_featured_image("table") ?></td>
				<td><?php echo $item->get_name() ?></td>
				<td><?php echo $item->get_price_per_piece($order->get_currency()) . " " . $order->get_currency()->get_symbol() ?></td>
				<td><?php echo $item->get_quantity() ?></td>
				<td><?php echo $item->get_sum() . " " . $order->get_currency()->get_symbol() ?></td>
			</tr>
		<?php endforeach; ?>
		<tr class="footer">
			<td></td>
			<td></td>
			<td></td>
			<td><?php _e("Celková cena", "eso") ?></td>
			<td><?php echo $order->get_sum_with_original_currency() ?></td>
		</tr>
	</table>
	<a class="button"
	   href="<?php $order->the_edit_url() ?>"><?php _e( "Zobrazit v eshopu", "eso" ) ?></a>
<?php eso_email_template_footer(); ?>