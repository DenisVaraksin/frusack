<?php
/*
Template Name: Obchodni Podminky
Template Post Type: page
*/
get_header();?>

    <main role="main">


<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <section id="contact-frusack">
        <!-- content is being rendered in Gutenberg Main Page -->
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="obchodni-header">
                        <h1><?php echo get_the_title(eso_get_page_id('Obchodní podmínky'))?></h1>
                    </div>
                </div>
            </div>
        </div>

        <?php the_content(); ?>

    </section>
    </main>

<?php  endwhile; endif; ?>


<?php get_footer(); ?>