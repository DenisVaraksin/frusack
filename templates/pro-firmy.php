<?php
/*
Template Name: Pro Firmy
Template Post Type: page
*/
get_header();?>

    <main role="main">


<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <section id="contact-frusack">
        <!-- content is being rendered in Gutenberg Main Page -->
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-5">
                    <?php echo do_shortcode('[contact-form-7 id="201" title="Contact form 1" html_class="custom-contact-form"]'); ?>
                </div>
                <div class="col-md-6">
                </div>
            </div>

        </div>



    </section>
    </main>

<?php  endwhile; endif; ?>


<?php get_footer(); ?>