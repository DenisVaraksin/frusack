<div class="row">
    <div class="col">
        <h2><?php _e( "Souhrn objednávky", "eso" ) ?></h2>
    </div>
</div>
<div class="row checkout-group" id="checkout-summary">
	<?php
	$checkout_fields = new Eso_Checkout_Fields();
	$checkout_fields->render_summary();
	?>
</div>
<div class="row">
    <div class="col">
        <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
           class="btn btn-lg btn-link"><?php _e( "Zpět nakupovat", "eso" ) ?></a>
    </div>
    <div class="col text-md-right">
        <button type="submit" id="eso-cart-submit " class="btn btn-lg btn-primary" name="eso-cart-submit">
	        <?php _e( "Potvrdit objednávku", "eso" ) ?>
        </button>
    </div>
</div>