<?php
$cart = new Eso_Cart( eso_session_token() );
/* @var $active_currency Eso_Currency */
$active_currency = eso_get_active_currency();
$checkout_fields = new Eso_Checkout_Fields();

if ( $cart->has_items() ) : ?>
    <table class="table table-responsive" id="cart-content-table">
        <tbody>
        <tr>
            <th></th>
            <th>Produkt</th>
            <th>Cena</th>
            <th>Množství</th>
            <th>Cena celkem</th>
            <th></th>
        </tr>
		<?php
		/* @var $cart_item Eso_Cart_Item */
		foreach ( $cart->get_items() as $cart_item ) :
			$product = new Eso_Product( $cart_item->get_id() );
			?>
            <tr class="product-row">
                <td>
					<?php if ( $product->get_featured_image_id() ) : ?>
						<?php echo wp_get_attachment_image( $product->get_featured_image_id(), "table" ) ?>
					<?php endif; ?>
                </td>
                <td>
					<?php echo $product->get_name(); ?>
                </td>
                <td>
                    <span>
                      <?php echo $cart_item->get_product()->get_price( eso_get_active_currency(), true, true ) ?> /  ks
                    </span>
                </td>
                <td class="form-inline pt-4">
                    <label>
						<?php
						/* @var $currency Eso_Currency */
						foreach ( eso_get_all_currencies() as $currency ) : ?>
                            <input type="hidden"
                                   name="checkout[product][<?php echo $cart_item->get_id() ?>][<?php echo $currency->get_code() ?>][price_per_piece]"
                                   value="<?php echo $product->get_price( $currency ) ?>">
                            <input type="hidden"
                                   name="checkout[product][<?php echo $cart_item->get_id() ?>][tax_rate]"
                                   value="<?php echo $product->get_tax_rate( eso_get_active_currency() )->get_id() ?>">
						<?php endforeach; ?>
                        <input type="number" min="1" max="<?php echo $product->get_stock_amount() + $cart_item->get_quantity() ?>"
                               class="form-control form-control--digit eso-cart-item-quantity-change"
                               data-id="<?php echo $cart_item->get_id() ?>"
                               data-product-name="<?php echo $product->get_name() ?>"
                               name="checkout[product][<?php $cart_item->the_id() ?>][quantity]"
                               value="<?php echo $cart_item->get_quantity(); ?>"/>
                        <span class="eso-quantity-label p-4"><?php _e( "ks", "eso" ) ?></span>
                    </label>
                </td>
                <td>
                    <span class="eso-cart-item-value eso-cart-item-value-<?php $cart_item->the_id() ?>"><?php echo $cart_item->get_total_with_currency( true ) ?></span>
                </td>
                <td>
                    <a href="#" class="eso-cart-item-remove" data-id="<?php $cart_item->the_id() ?>"><i
                                class="fas fa-times fa-2x"></i></a>
                </td>
            </tr>
		<?php endforeach ?>
        </tbody>
    </table>
    <div id="cart-sum-table">
	    <?php $checkout_fields->render_cart_sum() ?>
    </div>

    <div class="empty-cart-message">
        <p><?php _e( "V košíku nejsou žádné položky.", "eso" ) ?></p>
        <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
           class="btn btn-primary"><?php _e( "Procházet zboží", "eso" ) ?></a>
    </div>
    <div class="cart-summary text-right">
        <table class="table table-responsive">
            <tr></tr>
        </table>
    </div>
    <div class="cart-actions">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
                       class="btn btn-link"><?php _e( "Procházet zboží", "eso" ) ?></a>
                </div>
                <div class="col text-md-right">
                    <a href="#cart-customer"
                       class="btn btn-lg pill-move-next btn-secondary "><?php _e( "Dokončit objednávku", "eso" ) ?></a>
                </div>
            </div>
        </div>
    </div>

<?php
else : ?>
    <p><?php _e( "V košíku nejsou zatím žádné položky.", "eso" ) ?></p>
    <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
       class="btn btn-primary"><?php _e( "Procházet zboží", "eso" ) ?></a>
<?php endif; ?>