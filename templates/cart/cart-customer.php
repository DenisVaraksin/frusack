<?php
$checkout_fields = new Eso_Checkout_Fields();
if ( is_user_logged_in() ) {
	$customer = new Eso_Customer( get_current_user_id() );
}

$session = new Eso_Session( eso_session_token() );
$checkout_fields->render_customer_fields();
?>
<hr/>
<div class="row checkout-group">
    <div class="col-md" id="checkout-shipment">
		<?php $checkout_fields->render_shipment_fields() ?>
    </div>
    <div class="col-md" id="checkout-payment">
		<?php $checkout_fields->render_payment_fields() ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="order_note"><?php _e( "Poznámka k objednávce", "eso" ) ?></label>
            <textarea class="form-control" name="checkout[customer][order_note]" id="order_note"
                      placeholder="<?php _e( "Poznámka k objednávce", "eso" ) ?>"
                      rows="6"><?php if ( $session && ! empty( $session->get_value( "order_note" ) ) )
					echo $session->get_value( "order_note" ) ?></textarea>
        </div>
    </div>
    <div class="col-sm-6">
        <div id="coupon">
            <h3><label for="coupon-code"><?php _e( "Mám slevový kód", "eso" ) ?></label></h3>
            <input type="text" name="checkout[options][coupon_code]" id="coupon-code"
                   class="form-control form-control--short d-inline-block novalidate mb-2" placeholder="Kód"
                   value="<?php if ( isset( $customer ) ) {
				       echo $customer->get_meta( "coupon-code" );
			       } ?>"/>
            <button type="button"
                    class="btn btn-sm btn-secondary"><?php _e( "Uplatnit", "eso" ) ?></button>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-6 offset-sm-6">
        <div class="cart-totals">
			<?php $checkout_fields->render_cart_totals() ?>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col">
        <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
           class="btn btn-lg btn-link"><?php _e( "Zpět nakupovat", "eso" ) ?></a>
    </div>
    <div class="col text-md-right">
        <a href="#cart-summary" id="checkout-move-to-summary"
           class="btn btn-lg pill-move-next btn-secondary"><?php _e( "Pokračovat na souhrn", "eso" ) ?></a>
    </div>
</div>
