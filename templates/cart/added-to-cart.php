<?php
/**
 * @since 2019.7
 */
$cart = new Eso_Cart( eso_session_token() );

?>
<div class="modal fade" id="added-to-cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php _e( "Zboží bylo přidáno do košíku", "eso" ) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <p><?php _e( "Právě máte v košíku: ", "eso" ) ?></p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid product-list">
					<?php
					/* @var $cart_item Eso_Cart_Item */
					foreach ( $cart->get_items() as $cart_item ) :
						$product = new Eso_Product( $cart_item->get_id() );
						?>
                        <div class="row product-list__row">
                            <div class="col-sm-2">
								<?php if ( $product->get_featured_image_id() ) :
									echo wp_get_attachment_image( $product->get_featured_image_id(), "table" );
								else :
									echo eso_empty_thumbnail();
								endif; ?>
                            </div>
                            <div class="col d-flex align-items-center">
								<?php echo $product->get_name(); ?>
                            </div>
                            <div class="col d-flex align-items-center">
								<?php echo $cart_item->get_quantity() . " " . __( "ks", "eso" ) ?>
                            </div>
                            <div class="col-sm-3 d-flex align-items-center justify-content-end">
								<?php echo $cart_item->get_total_with_currency( true ) ?>
                            </div>
                        </div>
					<?php endforeach ?>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
                   class="btn btn-link">
                    <?php _e( "Pokračovat v nákupu", "eso" ) ?>
                </a>
                <a href="<?php eso_the_page_link( "cart" ) ?>"
                   class="btn btn-primary"><?php _e( "Přejít do košíku", "eso" ) ?></a>
            </div>
        </div>
    </div>
</div>
