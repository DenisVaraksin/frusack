<?php
/**
 * Block Name: Front Slider
 *
 * @author Denis Varaksin
 * @since 12.20.2019
 */

//creating class atribute for custom "className"
$className = 'front-slider';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}


//loading values and assigning defaults
//$slider_picture = get_field('slider-image');
?>
<div class="container-fluid remove-padding" id="homepage-slider-container">
    <div class="row">
    <div class="col">

        <div class="<?php echo esc_attr($className);?>">
            <div class="slider" id="homepage-slider">
                <?php if (have_rows('front-slider')) :
                    while (have_rows('front-slider')) : the_row();
                    $slider_picture = get_sub_field('slider-image');
                    $slider_header = get_sub_field('slider-header');
                    $slider_header2 = get_sub_field('slider-header-2');
                    $slider_text = get_sub_field('slider-text');
                    $slider_button = get_sub_field('front-slider-button');
                    $slider_button_link = get_sub_field('front-slider-button-link');
                ?>
                <div class="slides">
                    <div class="slider_picture">
                        <img src="<?php echo $slider_picture?>" alt="slider-picture" />
                        <div class="slides-content">
                            <div class="slides-content-header-1">
                                <?php echo $slider_header?>
                            </div>
                            <div class="slides-content-header-2">
                                <?php echo $slider_header2?>
                            </div>
                            <div class="slides-content-text">
                                <?php echo $slider_text?>
                            </div>
                            <div class="slider-content-button">
                                <a href="<?php echo $slider_button_link?>" class="btn btn-secondary"><?php echo $slider_button ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                endwhile;
                endif;
                ?>
            </div>
        </div>

    </div>
    </div>
</div>

