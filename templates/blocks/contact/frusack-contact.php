<?php
/**
 * Block Name: Frusack Contact
 *
 * @author Denis Varaksin
 * @since 01.14.2020
 */

//creating class atribute for custom "className"
$className = 'frusack-contact';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
    $frusackHeader1 = get_field('frusack-contact-header-1');
    $frusackText1 = get_field('frusack-contact-text-1');
    $frusackHeader2 = get_field('frusack-contact-header-2');
    $frusackAddress = get_field('frusack-contact-address');
    $frusackPostcode = get_field('frusack-contact-postcode');
    $frusackEmail = get_field('frusack-contact-email');
    $frusackOffer = get_field('frusack-contact-offer');
?>
<div class="container-fluid remove-padding">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-4">
            <div class="frusack-contact-form-wrapper">
                <div class="frusack-contact-header-1">
                    <h1><?php echo $frusackHeader1 ?></h1>
                </div>
                <div class="frusack-contact-text-1">
                    <?php echo $frusackText1 ?>
                </div>
                <div class="frusack-contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="201" title="Contact form 1" html_class="custom-contact-form"]'); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5 col">
            <div class="frusack-contact-data">
                <div class="frusack-contact-header-2">
                    <h2><?php echo $frusackHeader2 ?></h2>
                </div>
                <div class="frusack-contact-address">
                    <?php echo $frusackAddress ?>
                </div>
                <div class="frusack-contact-postcode">
                    <?php echo $frusackPostcode ?>
                </div>
                <div class="frusack-contact-email">
                    <?php echo $frusackEmail ?>
                </div>
                <div class="frusack-contact-offer">
                    <?php echo $frusackOffer ?>
                </div>
            </div>
        </div>
    </div>
</div>

