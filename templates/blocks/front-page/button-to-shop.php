<?php
/**
 * Block Name: Frusack Button to Eshop
 *
 * @author Denis Varaksin
 * @since 01.20.2020
 */

//creating class atribute for custom "className"
$className = 'frusack-is-advantages';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}


?>
<div class="container-fluid remove-padding" id="homepage-button-toshop">
    <div class="row">
        <div class="col">
            <?php $buttontext1 = get_field('frusack-toshop-button-text')?>
            <div class="button-wrapper custom-button-wrapper">
                <a class="btn btn-primary custom-button-padding" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Produkty' ) ) ); ?>"><?php echo $buttontext1 ?></a>
            </div>
        </div>
    </div>
</div>