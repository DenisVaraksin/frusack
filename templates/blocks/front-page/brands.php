<?php
/**
 * Block Name: Related Brands
 *
 * @author Denis Varaksin
 * @since 01.13.2020
 */

//creating class atribute for custom "className"
$className = 'front-brands';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
$brand_header = get_field('front-brand-header');
?>
<div class="container-fluid remove-padding">
    <div class="row">
        <div class="col">
            <div class="front-brand-header">
                <h2><?php echo $brand_header ?></h2>
            </div>
        </div>
    </div>
</div>

<div id="custom-grid-7">
    <div class="custom-row">
        <?php if (have_rows('front-brand')) :
            while (have_rows('front-brand')) : the_row();
                $brand_picture = get_sub_field('brand-image');
                ?>
                <div class="custom-col-1">
                    <img src="<?php echo $brand_picture?>" alt="brand-picture"/>
                </div>
            <?php
            endwhile;
        endif;
        ?>
    </div>
</div>