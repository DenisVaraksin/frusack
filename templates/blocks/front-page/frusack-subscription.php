<?php
/**
 * Block Name: Frusack Subscription
 *
 * @author Denis Varaksin
 * @since 01.08.2020
 */

//creating class atribute for custom "className"
$className = 'frusack-subscription';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}

$picture = get_field('subscription-background');
$header = get_field('subscription-header');
$text = get_field('subscription-text');
$policy1 = get_field('subscription-policy');
$policy2 = get_field('subscription-policy-link');
?>
<div class="container-fluid remove-padding" id="homepage-subscription-container" style="background:url(<?php echo $picture ?>); background-repeat: no-repeat; background-size: cover; background-position: center left; min-height: 530px;">
    <div class="row" >
        <div class="col-12">
            <div class="subscription-header">
                <h2><?php echo $header; ?></h2>
            </div>
            <div class="subscription-text">
                <?php echo $text; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="subscription-input">
                <?php echo do_shortcode('[eso_newsletter_signup show_terms=1]'); ?>
            </div>
        </div>
    </div>
</div>