<?php
/**
 * Block Name: Frusack Is Advantages
 *
 * @author Denis Varaksin
 * @since 12.20.2019
 */

//creating class atribute for custom "className"
$className = 'frusack-is-advantages';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}


//loading values and assigning defaults
//$slider_picture = get_field('slider-image');
?>
<div class="container-fluid remove-padding" id="homepage-slider-container">
    <div class="row">
        <?php if (have_rows('frusack-is-advantages')) :
        while (have_rows('frusack-is-advantages')) : the_row(); ?>
        <div class="col-12 col-sm-6 col-lg-4 p-unset">
            <div class="<?php echo esc_attr($className);?>">
                            <?php
                            $picture = get_sub_field('frusack-is-image');
                            $header = get_sub_field('frusack-is-header');
                            $text = get_sub_field('frusack-is-text');
                            ?>
                <div class="frusack-is-image">
                    <img src="<?php echo $picture?>" alt="advantages-picture" />
                </div>
                <div class="frusack-is-header">
                    <h3><?php echo $header ?></h3>
                </div>
                <div class="frusack-is-text-advantages">
                    <?php echo $text ?>
                </div>
            </div>
        </div>
        <?php
        endwhile;
        endif;
        ?>
    </div>
    <div class="row">
        <div class="col">
            <?php $buttontext = get_field('frusack-is-button-text')?>
            <div class="button-wrapper">
                <a class="btn btn-primary" href="<?php echo get_page_link(19); ?>"><?php echo $buttontext ?></a>
            </div>
        </div>
    </div>
</div>