<?php
/**
 * Block Name: Frusack-Is
 *
 * @author Denis Varaksin
 * @since 01.07.2020
 */

//creating class atribute for custom "className"
$className = 'frusack-is';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
?>

<div class="container-fluid remove-padding" id="block-frusack-is">
    <div class="row">
        <div class="col">
            <div class="<?php echo esc_attr($className);?>">

                            <?php
                            $text = get_field('frusack-is');
                            ?>
                            <div class="frusack-is-content">
                                <div class="frusack-is-text">
                                    <h2><?php echo $text ?></h2>
                                </div>
                            </div>
            </div>
        </div>
    </div>
</div>
