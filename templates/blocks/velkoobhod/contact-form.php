<?php
/**
 * Block Name: Frusack Velkoobhod Contact Form
 *
 * @author Denis Varaksin
 * @since 02.17.2020
 */
?>
<?php
$header =  get_field('velkoobhod-contact-header');
$subheader = get_field('velkoobhod-contact-subheader');
?>

<section id="velkoobhod-contact" class="remove-padding">
    <img src="<?php echo get_template_directory_uri() . "/img/roses_transparent.png" ?>" alt="flowers-image">

    <div class="container-fluid custom-margin">
        <div class="row">
            <div class="col">
                <div class="velko-contact-wrapper">
                    <div class="velko-contact-header">
                        <h3><?php echo $header; ?></h3>
                    </div>
                    <div class="velko-contact-separator"></div>
                    <div class="velko-contact-subheader">
                        <?php echo $subheader; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">
                <?php echo do_shortcode('[contact-form-7 id="528" title="Contact form 2" html_class="custom-contact-form"]'); ?>
            </div>
            <div class="col-md-5">
            </div>
        </div>
    </div>
</section>

