<?php
/**
 * Block Name: Frusack Velkoobhod Advantages
 *
 * @author Denis Varaksin
 * @since 02.13.2020
 */
?>

<section id="velkoobhod-advantages">
    <div class="container-fluid">
        <div class="row">
            <?php if (have_rows('velkoobhod-advantages')) :
                while (have_rows('velkoobhod-advantages')) : the_row(); ?>
                    <div class="col-12 col-sm-6 col-lg-4 p-unset">
                        <div class="velko-advantages-wrapper">
                            <?php
                            $picture = get_sub_field('advantages-picture');
                            $picture_hover = get_sub_field('advantages-picture-hover');
                            $header = get_sub_field('advantages-header');
                            $text = get_sub_field('advantages-text');
                            ?>

                            <div class="velko-advantages-image">
                                <img class="velko-advantages-actual-image" src="<?php echo $picture;?>" alt="velko-advantages-image">
                                <img class="velko-advantages-actual-imagehover d-none" src="<?php echo $picture_hover;?>" alt="velko-advantages-image">
                            </div>

                            <div class="velko-advantages-header">
                                <h2><?php echo $header; ?></h2>
                            </div>
                            <div class="velko-advantages-text">
                                <?php echo $text; ?>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>

    </div>
</section>
