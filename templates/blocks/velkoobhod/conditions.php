<?php
/**
 * Block Name: Frusack Velkoobhod Conditions
 *
 * @author Denis Varaksin
 * @since 02.14.2020
 */
?>
<section id="velkoobhod-conditions">
    <div class="container-fluid">
        <div class="row">
            <?php if (have_rows('velkoobhod-conditions')) :
            while (have_rows('velkoobhod-conditions')) : the_row(); ?>
            <div class="col-12 col-sm-6 col-lg-4 p-unset">
                <div class="velko-conditions-wrapper">
                    <div class="conditions-add-wrapper">
                        <?php
                        $picture = get_sub_field('conditions-picture');
                        $header = get_sub_field('conditions-header');
                        $text = get_sub_field('conditions-text');
                        ?>
                        <div class="velko-conditions-image">
                            <img class="velko-conditions-actual-image" src="<?php echo $picture;?>" alt="velko-conditions-image">
                        </div>
                        <div class="velko-conditions-header">
                            <h3><?php echo $header; ?></h3>
                        </div>
                        <div class="velko-conditions-text">
                            <span><?php echo $text; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
