<?php
/**
 * Block Name: Frusack Velkoobhod Header
 *
 * @author Denis Varaksin
 * @since 02.13.2020
 */
?>

<?php
    $image =  get_field('velkoobhod-header-image');
    $text = get_field('velkoobhod-header-text');
?>

<section id="velkoobhod-header" style="background: url(<?php echo $image ?>)">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="text-wrapper">
                    <h1><?php echo $text; ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>