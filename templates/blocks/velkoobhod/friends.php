<?php
/**
 * Block Name: Frusack Velkoobhod Reference
 *
 * @author Denis Varaksin
 * @since 02.17.2020
 */
?>

<?php
$header =  get_field('velkoobhod-friends-header');
?>

<section id="velkoobhod-friends">
   <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="velko-friends-header-wrapper">
                    <div class="velko-friends-header">
                        <h3><?php echo $header; ?></h3>
                    </div>
                    <div class="velko-friends-separator">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="custom-grid-5">
        <div class="custom-row">
            <?php if (have_rows('velkoobhod-friends-logos')) :
                while (have_rows('velkoobhod-friends-logos')) : the_row();
                    $brand_picture = get_sub_field('brand-image');
                    ?>
                    <div class="custom-col-2">
                        <img src="<?php echo $brand_picture?>" alt="brand-picture"/>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
