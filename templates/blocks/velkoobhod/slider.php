<?php
/**
 * Block Name: Frusack Velkoobhod Slider
 *
 * @author Denis Varaksin
 * @since 02.17.2020
 */
?>
<?php
$header =  get_field('velkoobhod-slider-header');
?>

<section id="velkoobhod-slider">
    <img class="velko-slider-back-pic" src="<?php echo get_template_directory_uri() . "/img/Layer3.png" ?>" alt="fruits-picture">
    <div class="container custom-margin">
        <div class="row">
            <div class="col">
                <div class="velkoobhod-slider-header-wrapper">
                    <div class="velkoobhod-slider-header">
                        <h3><?php echo $header; ?></h3>
                    </div>
                    <div class="velkoobhod-slider-separator"></div>
                </div>
                <div class="velkoobhod-slider">
                    <div class="velko-slides-wrap ">
                        <?php if (have_rows('velkoobhod-slider')) :
                        while (have_rows('velkoobhod-slider')) : the_row();
                        $slider_picture = get_sub_field('slider-image');
                        $slider_header = get_sub_field('slider-header');
                        $slider_header2 = get_sub_field('slider-header-2');
                        $slider_text = get_sub_field('slider-text');
                        $slider_button = get_sub_field('slider-button');
                        $slider_button_link = get_sub_field('slider-button-link');
                        ?>
                        <div class="velko-slides">
                            <img src="<?php echo $slider_picture?>" alt="slider-picture" />
                            <div class="velko-slides-content">
                                <div class="velko-slides-content-header-1">
                                    <?php echo $slider_header?>
                                </div>
                                <div class="velko-slides-content-header-2">
                                    <?php echo $slider_header2?>
                                </div>
                                <div class="velko-slides-content-text">
                                    <?php echo $slider_text?>
                                </div>
                                <div class="velko-slider-content-button">
                                    <a href="<?php echo $slider_button_link; ?>" target="_blank" class="button-green"><?php echo $slider_button ?></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        endwhile;
                        endif;
                        ?>
                    </div>
                    <div class="prev"><img src="<?php echo get_template_directory_uri() . "/img/prev.png" ?>" alt="prev-image"></div>
                    <div class="next"><img src="<?php echo get_template_directory_uri() . "/img/next.png" ?>" alt="next-image"></div>
                </div>
            </div>
        </div>
    </div>
</section>
