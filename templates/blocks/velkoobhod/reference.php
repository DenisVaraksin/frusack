<?php
/**
 * Block Name: Frusack Velkoobhod Reference
 *
 * @author Denis Varaksin
 * @since 02.13.2020
 */
?>

<?php
$image =  get_field('velkoobhod-reference-image');
$header = get_field('velkoobhod-reference-header');
$text = get_field('velkoobhod-reference-text');
$button = get_field('velkoobhod-reference-button');
?>
<section id="velkoobhod-reference">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-8">
                <div class="velko-reference-image">
                    <img src="<?php echo $image; ?>" alt="reference-image">
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="velko-reference-content-wrapper">
                    <div class="velko-reference-header">
                        <h3><?php echo $header; ?></h3>
                    </div>
                    <div class="velko-reference-text">
                        <?php echo $text; ?>
                    </div>
                    <div class="velko-reference-button-wrapper">
                        <button class="button-green" id="velko-reference-button" data-toggle="modal" data-target="#velkoModal1"><?php echo $button; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$header1 = get_field('velkoobhod-model-header1');
$image1 =  get_field('velkoobhod-model-pic1');
$image2 =  get_field('velkoobhod-model-pic2');
$image3 =  get_field('velkoobhod-model-pic3');
$button1 = get_field('velkoobhod-model-button1');
$button2 = get_field('velkoobhod-model-button2');
$button3 = get_field('velkoobhod-model-button3');

$header2 = get_field('velkoobhod-model-header2');

$header3 = get_field('velkoobhod-model-header3');
$text1 = get_field('velkoobhod-model-text');
$button5 = get_field('velkoobhod-model-button5');
?>

<!-- Modal Window First -->
<div class="modal fade bd-example-modal-lg" id="velkoModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?php echo $header1; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="modal-exit" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="body-section">
                    <div class="body-section-pic">
                        <img src="<?php echo $image1; ?>" alt="supermarket">
                    </div>
                    <div class="body-section-text">
                        <button class="button-green open-model-2" ><?php echo $button1; ?></button>    <!-- data-toggle="modal" data-target="#velkoModal2" -->
                    </div>
                </div>
                <div class="body-section">
                    <div class="body-section-pic">
                        <img src="<?php echo $image2; ?>" alt="distributor">
                    </div>
                    <div class="body-section-text">
                        <button class="button-green open-model-2" ><?php echo $button2; ?></button>
                    </div>
                </div>
                <div class="body-section">
                    <div class="body-section-pic">
                        <img src="<?php echo $image3; ?>" alt="lokální obchod">
                    </div>
                    <div class="body-section-text">
                        <button class="button-green open-model-2" ><?php echo $button3; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Window Second -->
<div class="modal fade bd-example-modal-lg" id="velkoModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?php echo $header2; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="modal-exit" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[contact-form-7 id="574" title="Contact form 3" html_class="custom-contact-form-modal"]'); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Window Third -->
<div class="modal fade bd-example-modal-lg" id="velkoModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?php echo $header3; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="modal-exit" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="body-section">
                    <div class="body-section-text">
                        <span class="d-block mb-5"><?php echo $text1; ?></span>
                        <button class="button-green" data-dismiss="modal"><?php echo $button5; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>