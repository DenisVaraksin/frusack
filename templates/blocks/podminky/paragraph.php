<?php
/**
 * Block Name: Obchodny Podminky Paragraph
 *
 * @author Denis Varaksin
 * @since 01.16.2020
 */

//creating class atribute for custom "className"
$className = 'obchodny-paragraph';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];}

    $obchodni_header = get_field('obchodny-paragraph-header');


?>
<div class="container" >
    <div class="row">
        <div class="col">
            <div class="obchodny-header">
                <h2><?php echo $obchodni_header ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col mb-4">
            <?php $i = 1;
            if (have_rows('obchodny-paragraph-block')) :
            while (have_rows('obchodny-paragraph-block')) : the_row();
            $obchodny_block_text = get_sub_field('paragraph-text');
            ?>
                <div class="obchodny-block">
                    <div class="obchodny-block-number ">
                        <?php echo $i ?>.
                    </div>
                    <div class="obchodny-block-text ">
                        <?php echo $obchodny_block_text ?>
                    </div>
                </div>
            <?php
            $i = $i + 1;
            endwhile;
            endif;
            ?>
        </div>
    </div>
</div>

