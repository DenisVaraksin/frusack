<?php
/**
 * Block Name: Obchodny Podminky Text
 *
 * @author Denis Varaksin
 * @since 01.16.2020
 */


$obchonyText = get_field('obchodny-text');

?>
<div class="container" >
    <div class="row">
        <div class="col">
            <div class="obchodny-text">
                <?php echo $obchonyText ?>
            </div>
        </div>
    </div>
</div>
