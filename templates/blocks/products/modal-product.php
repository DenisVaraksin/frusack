<?php
/**
 * Block Name: Frusack Single Product For a Modal Window(s)
 *
 * @author Denis Varaksin
 * @since 03.02.2020
 */
?>

    <!-- displaying products of certain category -->
    <?php
    $product = new Eso_Product( get_the_ID() );
    $product_cat = $product->get_category_id();
    ?>

    <!-- Modal for widescreen -->
    <div class="container" id="single-product-container">
        <div class="row">
            <div class="col-sm-7">
                <div class="product-gallery">
                    <div class="product-gallery__main">
                        <?php if ( $product->get_featured_image_id() ) : ?>
                            <a href="<?php echo wp_get_attachment_image_url( $product->get_featured_image_id(), "photo" ) ?>"
                               data-lightbox="<?php echo $product->get_id() ?>">
                               <?php $product->the_featured_image( "large" ); ?>
                            </a>
                        <?php else:
                            echo eso_empty_thumbnail();
                        endif; ?>
                    </div>
                    <?php
                    $images = $product->get_images( false );
                    if ( ! empty( $images ) ) {
                        foreach ( $images as $key => $image_id ) { ?>
                            <div class="product-gallery__item">
                                <a href="<?php echo wp_get_attachment_image_url( $image_id, "photo" ) ?>"
                                   data-lightbox="<?php echo $product->get_id() ?>">
                                    <?php echo wp_get_attachment_image( $image_id, 'extrasmall' ); ?>
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="product-detail__info">
                    <h1><?php the_title();
                        if ( ! empty( $product->get_tag_id() ) ) {
                            $product_tag = new Eso_Product_Tag( $product->get_tag_id() );
                            $product_tag->render( "product-detail" );
                        } ?>
                    </h1>

                    <?php if ( $product->is_purchasable() ) : ?>
                    <p class="product-detail__header">
                        <?php if ( $product->is_discounted( eso_get_active_currency() ) ) : ?>
                            <span class="price-before-discount"><?php echo $product->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
                        <?php endif; ?>
                        <span class="price__item"><?php echo $product->get_price( eso_get_active_currency(), true, true ); ?></span>
                    </p>

                    <div class="product-detail__cta">

                        <div class="single-product-colors">
                            <div class="product-colors-title">
                                Barva
                            </div>

                            <div class="product-colors <?php $category = $product->get_category_name();
                                echo $category . 'Modified'; ?>">
                                <img type=""
                                     src="<?php echo $product->get_symbol_image_url(); ?>"
                                     alt="product-color">
                            </div>
                        </div>



                        <form action="" id="single-add-to-cart" class="add-to-cart-form"
                              data-id="<?php the_ID() ?>">
                            <?php wp_nonce_field( ESO_NONCE ); ?>
                            <div class="form-row">
                                <div class="col-auto mt-auto mb-auto">
                                    <div class="produc-amount">
                                        Počet ks
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <label class="product-details__label">
                                        <input type="number"
                                               class="form-control form-control-lg eso-product-quantity"
                                               name="eso-product-quantity" min="1" required value="1"
                                               data-product-name="<?php echo $product->get_name() ?>"
                                               max="<?php echo $product->get_stock_amount() ?>"/>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <button type="submit" class="btn-custom-white add-to-cart">
                                        <?php _e( "přidat do košíku", "eso" ) ?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/shop-cart.png" alt="">
                                    </button>
                                </div>
                            </div>

                        </form>
                        <?php else : ?>
                            <div class="stock-status stock-status--<?php echo $product->get_stock_status_slug() ?>">
                                <span class="stock-status__name"><?php echo $product->get_stock_status_name() ?></span>
                            </div>
                        <?php endif; ?>
                    </div>

                    <p>
                        <?php the_content(); ?>
                    </p>
                    <?php the_tags( __( 'Tags: ', 'theme' ), ', ', '<br>' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- --------------------  -->
    <!-- Modal for smallscreen -->
    <!-- screen smaller 575px  -->

<div class="container" id="single-product-container-small">
    <div class="row">
        <div class="col">   <!-- col-sm-7 -->
            <div class="product-detail__info mt-3">
                <h1>
                    <?php the_title();
                    if ( ! empty( $product->get_tag_id() ) ) {
                        $product_tag = new Eso_Product_Tag( $product->get_tag_id() );
                        $product_tag->render( "product-detail" );
                    } ?>
                </h1>
            </div>


            <div class="product-gallery mt-3 mb-3">
                <div class="product-gallery__main">
                    <?php if ( $product->get_featured_image_id() ) : ?>
                        <a href="<?php echo wp_get_attachment_image_url( $product->get_featured_image_id(), "photo" ) ?>"
                           data-lightbox="<?php echo $product->get_id() ?>">
                            <?php $product->the_featured_image( "large" ); ?>
                        </a>
                    <?php else:
                        echo eso_empty_thumbnail();
                    endif; ?>
                </div>
            </div>
         <!-- col-sm-5 -->
            <div class="product-detail__info">


                <?php if ( $product->is_purchasable() ) : ?>
                <p class="product-detail__header">
                    <?php if ( $product->is_discounted( eso_get_active_currency() ) ) : ?>
                        <span class="price-before-discount"><?php echo $product->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
                    <?php endif; ?>
                    <span class="price__item"><?php echo $product->get_price( eso_get_active_currency(), true, true ); ?></span>
                </p>

                <div class="product-detail__cta">

                    <div class="single-product-colors">
                        <div class="product-colors-title">
                            Barva
                        </div>

                        <div class="product-colors <?php $category = $product->get_category_name();
                        echo $category . 'Modified'; ?>">
                            <img type=""
                                 src="<?php echo $product->get_symbol_image_url(); ?>"
                                 alt="product-color">
                        </div>
                    </div>



                    <form action="" id="single-add-to-cart" class="add-to-cart-form"
                          data-id="<?php the_ID() ?>">
                        <?php wp_nonce_field( ESO_NONCE ); ?>
                        <div class="form-row">
                            <div class="col-auto mt-auto mb-auto">
                                <div class="produc-amount">
                                    Počet ks
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="product-details__label">
                                    <input type="number"
                                           class="form-control form-control-lg eso-product-quantity"
                                           name="eso-product-quantity" min="1" required value="1"
                                           data-product-name="<?php echo $product->get_name() ?>"
                                           max="<?php echo $product->get_stock_amount() ?>"/>
                                </label>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn-custom-white add-to-cart">
                                    <?php _e( "přidat do košíku", "eso" ) ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/shop-cart.png" alt="">
                                </button>
                            </div>
                        </div>

                    </form>
                    <?php else : ?>
                        <div class="stock-status stock-status--<?php echo $product->get_stock_status_slug() ?>">
                            <span class="stock-status__name"><?php echo $product->get_stock_status_name() ?></span>
                        </div>
                    <?php endif; ?>
                </div>

                <p>
                    <?php the_content(); ?>
                </p>
                <?php the_tags( __( 'Tags: ', 'theme' ), ', ', '<br>' ); ?>
            </div>

            <div class="product-gallery">
            <?php
            $images = $product->get_images( false );
            if ( ! empty( $images ) ) {
                foreach ( $images as $key => $image_id ) { ?>
                    <div class="product-gallery__item">
                        <a href="<?php echo wp_get_attachment_image_url( $image_id, "photo" ) ?>"
                           data-lightbox="<?php echo $product->get_id() ?>">
                            <?php echo wp_get_attachment_image( $image_id, 'extrasmall' ); ?>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
            </div>
        </div>
    </div>
</div>
