<?php
/**
 * Block Name: Frusack Single Product
 *
 * @author Denis Varaksin
 * @since 01.24.2020
 */
?>
<div class="container" id="homepage-products">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-12 col-md-5 col-lg-4">
            <!-- Get a specific product by ID for Homepage display Frusack Duo -->
            <?php
            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_category',
                        'field' => 'id',
                        'terms' => 17
                    )
                )
            );

            $query = new WP_Query($args);
            if ($query->have_posts()) {
            $counter = 1;
            ?>
            <div class="product-category-wrap">
                <?php while ($query->have_posts()) {
                    $query->the_post();
                    $product = new Eso_Product(get_the_ID());
                    ?>

                    <!-- suppose to be <?php// the_permalink($product->get_id()); ?> inside of href="" -->

                    <a id="product-item-<?php echo get_the_ID(); ?>"
                       class="card__wrap product-item<?php if ($counter != 1) {
                           echo ' d-none';
                       } ?>">
                        <div>
                            <button type="button" class="modal-button" data-target="#theProductModal-<?php echo get_the_ID(); ?>" data-toggle="modal">
                                <div class="product-item__image">
                                    <?php if ($product->get_featured_image_id()) {
                                        $product->the_featured_image();
                                    } else {
                                        echo eso_empty_thumbnail();
                                    }
                                    ?>
                                    <div class="product-item__image-hover">
                                        <p>detail</p>
                                        <p class="bigger">+</p>
                                    </div>
                                </div>
                            </button>


                            <?php
                            if ($product->get_tag_id()) {
                                $product_tag = new Eso_Product_Tag($product->get_tag_id());
                                $product_tag->render("product-item");
                            }
                            ?>
                            <div class="product-item-title-wrap">
                                <h3 class="product-item__title"><?php echo $product->get_name(); ?></h3>
                            </div>

                            <div class="price price--list">
                                <?php if ($product->is_discounted(eso_get_active_currency())) : ?>
                                    <span
                                            class="price-before-discount"><?php echo $product->get_price_before_discount(eso_get_active_currency(), true) ?></span>
                                <?php endif; ?>
                                <span
                                        class="price__item"><?php echo $product->get_price(eso_get_active_currency(), true, true); ?></span>

                            </div>

                            <div class="product-item__purchase">
                                <div class="product-purchase">
                                    <form action="" class="add-to-cart-form"
                                          data-id="<?php echo $product->get_id() ?>">
                                        <?php wp_nonce_field(ESO_NONCE); ?>
                                        <div class="col-auto d-none">
                                            <label class="product-details__label">
                                                <input type="number"
                                                       class="form-control form-control-lg eso-product-quantity"
                                                       name="eso-product-quantity" min="1" required value="1"
                                                       data-product-name="<?php echo $product->get_name() ?>"
                                                       max="<?php echo $product->get_stock_amount() ?>"/>
                                            </label>
                                        </div>
                                        <div class="addtocart-button-wrapper">
                                            <button type="submit" class="btn-custom-white add-to-cart">
                                                <?php _e("do košíku", "eso") ?>
                                                <img
                                                        src="<?php echo get_template_directory_uri(); ?>/img/shop-cart.png"
                                                        alt="icon-cart">
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                    </a>

                    <!-- Modal window for a product quick view ( inside of the loop (?) ) -->
                    <div class="modal fade" id="theProductModal-<?php echo get_the_ID(); ?>">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md">
                                            <?php get_template_part('templates/blocks/products/modal-product'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $counter++;
                }
                }
                ?>
                <div class="colors-wrapper">
                    <?php
                    $count = 1;
                    while ($query->have_posts()) {
                        $query->the_post();
                        $product = new Eso_Product(get_the_ID());
                        ?>
                        <div class="product-color color-1">
                            <a class="product-color-trigger" href="<?php ?>"
                               data-target="<?php echo get_the_ID(); ?>">
                                <?php // echo $product->get_symbol_image(); ?>
                                <img type="" class="<?php if ($count == 1) : echo 'product-color-image-border'; endif; ?> "
                                     src="<?php echo $product->get_symbol_image_url(); ?>"
                                     alt="product-color">
                            </a>
                        </div>
                        <?php
                        $count++;
                    }
                    wp_reset_postdata();
                    ?>
                </div>


            </div>
        </div>

        <div class="col-md-2 col-lg-2"></div>

        <div class="col-12 col-md-5 col-lg-4">
            <!-- Get a specific product by ID for Homepage display Frusack Trio -->
            <?php
            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_category',
                        'field' => 'id',
                        'terms' => 18
                    )
                )
            );

            $query = new WP_Query($args);
            if ($query->have_posts()) {
            $counter = 1;
            ?>
            <div class="product-category-wrap">
                <?php while ($query->have_posts()) {
                    $query->the_post();
                    $product = new Eso_Product(get_the_ID());
                    ?>
                    <a id="product-item-<?php echo get_the_ID(); ?>"
                       class="card__wrap product-item<?php if ($counter != 1) {
                           echo ' d-none';
                       } ?>">
                        <div>
                            <button type="button" class="modal-button" data-target="#theProductModal-<?php echo get_the_ID(); ?>" data-toggle="modal">
                                <div class="product-item__image ">
                                    <?php if ($product->get_featured_image_id()) {
                                        $product->the_featured_image();
                                    } else {
                                        echo eso_empty_thumbnail();
                                    }
                                    ?>
                                    <div class="product-item__image-hover">
                                        <p>detail</p>
                                        <p class="bigger">+</p>
                                    </div>
                                </div>
                            </button>

                            <?php
                            if ($product->get_tag_id()) {
                                $product_tag = new Eso_Product_Tag($product->get_tag_id());
                                $product_tag->render("product-item");
                            }
                            ?>
                            <div class="product-item-title-wrap">
                                <h3 class="product-item__title"><?php echo $product->get_name(); ?></h3>
                            </div>

                            <div class="price price--list">
                                <?php if ($product->is_discounted(eso_get_active_currency())) : ?>
                                    <span
                                            class="price-before-discount"><?php echo $product->get_price_before_discount(eso_get_active_currency(), true) ?></span>
                                <?php endif; ?>
                                <span
                                        class="price__item"><?php echo $product->get_price(eso_get_active_currency(), true, true); ?></span>

                            </div>

                            <div class="product-item__purchase">
                                <div class="product-purchase">
                                    <form action="" class="add-to-cart-form"
                                          data-id="<?php echo $product->get_id() ?>">
                                        <?php wp_nonce_field(ESO_NONCE); ?>
                                        <div class="col-auto d-none">
                                            <label class="product-details__label">
                                                <input type="number"
                                                       class="form-control form-control-lg eso-product-quantity"
                                                       name="eso-product-quantity" min="1" required value="1"
                                                       data-product-name="<?php echo $product->get_name() ?>"
                                                       max="<?php echo $product->get_stock_amount() ?>"/>
                                            </label>
                                        </div>
                                        <div class="addtocart-button-wrapper">
                                            <button type="submit" class="btn-custom-white add-to-cart">
                                                <?php _e("do košíku", "eso") ?>
                                                <img
                                                        src="<?php echo get_template_directory_uri(); ?>/img/shop-cart.png"
                                                        alt="cart-icon">
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                    </a>
                    <!-- Modal window for a product quick view ( inside of the loop (?) ) -->
                    <div class="modal fade" id="theProductModal-<?php echo get_the_ID(); ?>">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md">
                                            <?php get_template_part('templates/blocks/products/modal-product'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $counter++;
                }
                }
                ?>
                <div class="colors-wrapper">
                    <?php
                    $count = 1;
                    while ($query->have_posts()) {
                        $query->the_post();
                        $product = new Eso_Product(get_the_ID());
                        ?>
                        <div class="product-color color-2">
                            <a class="product-color-trigger" href="<?php ?>"
                               data-target="<?php echo get_the_ID(); ?>">
                                <?php // echo $product->get_symbol_image(); ?>
                                <img type="" class="<?php if ($count == 1) : echo 'product-color-image-border'; endif; ?> "
                                     src="<?php echo $product->get_symbol_image_url(); ?>"
                                     alt="product-color">
                            </a>
                        </div>

                        <?php
                        $count++;
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
        <div class="col col-1 "></div>
    </div>
</div>
