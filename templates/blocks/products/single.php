<?php
/**
 * Block Name: Frusack Single Product Content
 *
 * @author Denis Varaksin
 * @since 01.22.2020
 */
?>

<div class="frusack-single-product-content">
    <?php
    $single_text = get_field('frusack-single-product-text');
    $single_characteristic1 = get_field('frusack-single-product-characteristic1');
    $single_characteristic2 = get_field('frusack-single-product-characteristic2');

    $single_header1 = get_field('frusack-single-product-header1');
    $single_text1 = get_field('frusack-single-product-text1');
    $single_header2 = get_field('frusack-single-product-header2');
    $single_text2 = get_field('frusack-single-product-text2');

    ?>
    <div class="frusack-single-product-text">
        <?php echo $single_text ?>
    </div>

    <div class="frusack-single-product-characteristic">
        <div class="frusack-single-product-characteristic1">
            <?php echo $single_characteristic1 ?>
        </div>
        <div class="frusack-single-product-characteristic2">
            <?php echo $single_characteristic2 ?>
        </div>
    </div>

    <div class="frusack-single-product-header1">
        <button type="button" class="single-product-button" data-toggle="collapse" data-target="#single-text-1"><span><?php echo $single_header1 ?></span></button>
    </div>
    <div class="frusack-single-product-text1 collapse" id="single-text-1">
        <?php echo $single_text1 ?>
    </div>

    <div class="frusack-single-product-header2">
        <button type="button" class="single-product-button" data-toggle="collapse" data-target="#single-text-2"><span><?php echo $single_header2 ?></span></button>
    </div>
    <div class="frusack-single-product-text2 collapse" id="single-text-2">
        <?php echo $single_text2 ?>
    </div>

</div>

