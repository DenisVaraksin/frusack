<?php
/**
 * Block Name: About Frusack Text
 *
 * @author Denis Varaksin
 * @since 09.01.2020
 */

//creating class atribute for custom "className"
$className = 'about-frusack-text';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
$text1 = get_field('about-frusack-text_1');
$text2 = get_field("about-frusack-text_2");

?>
<div class="container-fluid remove-padding">
    <div class="row">
        <div class="col">
            <div class="<?php echo esc_attr($className);?>">
                <div class="about-frusack-text-1">
                    <h1><?php echo $text1 ?></h1>
                </div>
                <div class="about-frusack-text-2">
                    <?php echo $text2 ?>
                </div>
            </div>
        </div>
    </div>
</div>
