<?php
/**
 * Block Name: Front Slider
 *
 * @author Denis Varaksin
 * @since 10.01.2020
 */

//creating class atribute for custom "className"
$className = 'about-frusack-content';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
$i = 1;
$widgetheader = get_field("about-frusack-content-header");
?>

<div class="container-fluid remove-padding" >




    <div class="row">

        <div class="col">
            <div class="about-content-picture-wrapper">
                <!-- envoking header pictures -->
                <?php
                $i = 1;
                if (have_rows('about-frusack-content-picture')) :
                    while (have_rows('about-frusack-content-picture')) : the_row();
                        $widgetpicture = get_sub_field('about-content-picture');
                        ?>
                        <div id="about-content-picture-<?php echo $i ?>"
                             class="about-content-picture  <?php if ($i != 1) { echo "d-none"; } ?> show-<?php echo $i;?>">
                            <img src="<?php echo $widgetpicture ?>" alt="widget-picture">
                        </div>
                        <?php
                        $i = $i + 1;
                    endwhile;
                endif;
                ?>

                <!-- envoking header text -->
                <?php
                $i = 1;
                if (have_rows('about-frusack-content-text')) :
                    while (have_rows('about-frusack-content-text')) : the_row();
                        $contentText = get_sub_field('about-content-text');
                        ?>
                        <div id="about-content-text-<?php echo $i ?>"
                             class="about-content-text <?php if ($i == 1) { echo "d-none"; } ?> show-<?php echo $i;?>">
                            <?php echo $contentText ?>
                        </div>
                        <?php
                        $i = $i + 1;
                    endwhile;
                endif;
                ?>
            </div>

        </div>

        <div class="col-md-12 ">
            <div class="about-content-header">
                <h1><?php echo $widgetheader ?></h1>
            </div>
        </div>


        <div class="col-md-4 col-12">
            <!-- envoking widget buttons -->
            <?php
            $i = 1;
            if (have_rows('about-frusack-content-widget')) :
            while (have_rows('about-frusack-content-widget')) : the_row();
            $widgettext = get_sub_field('content-widget-text');
            $widgetarticle = get_sub_field("content-widget-article");
            ?>
            <div class="about-content-wrapper-<?php echo $i ?> about-content " >
                <button type="button" class="about-content-button <?php if ($i == 1) { echo "active-button"; } ?>" data-attr="show-<?php echo $i; ?>">
                    <h2><?php echo $widgettext ?></h2>
                </button>
            </div>
            <?php
                $i = $i + 1;
            endwhile;
            endif;
            ?>
        </div>

        <div class="col-md-8 col-12">
            <!-- envoking widget content -->
            <?php
            $i = 1;
            if (have_rows('about-frusack-content-widget')) :
            while (have_rows('about-frusack-content-widget')) : the_row();
            $widgettext = get_sub_field('content-widget-text');
            $widgetarticle = get_sub_field("content-widget-article");

            ?>
            <div id="about-content-article-<?php echo $i; ?>" class="about-content-article mb-4 <?php if ($i != 1) { echo "d-none"; } ?> show-<?php echo $i;?>">
                <?php echo $widgetarticle ?>
            </div>
                <?php
                $i = $i + 1;
            endwhile;
            endif;
            ?>
        </div>

    </div>
</div>

