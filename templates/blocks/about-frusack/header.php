<?php
/**
 * Block Name: About Frusack Header
 *
 * @author Denis Varaksin
 * @since 09.01.2020
 */

//creating class atribute for custom "className"
$className = 'about-frusack-header';
if ( !empty($block[ 'className' ]) ) {
    $className .= ' ' . $block['className'];
}
$picture = get_field('about-frusack-header');

?>
<div class="container-fluid remove-padding" id="about-frusack-header-container-1">
    <div class="row">
        <div class="col">
            <div id="about-frusack-header-container"  style="background:url(<?php echo $picture ?>); background-repeat: no-repeat; background-size: cover; background-position: center right; ">

            </div>
        </div>
    </div>
</div>
